package main

import (
	"gopkg.in/yaml.v2"
	"os"
	"log"
	"flag"
	"io/ioutil"
)

func ReadConfig(path string, c interface{}) error {
	yamlFile, err := ioutil.ReadFile(path)
	if err == nil {
		err = yaml.Unmarshal(yamlFile, c)
	}
	return err
}

type Config struct {
	DB		string			`yaml:"db"`
	MQ		string			`yaml:"mq"`
	Volume		string			`yaml:"volume"`
}

var conf Config

func stopSelf() {
	/*
	 * If we're in Docker container, then exiting the main
	 * process stops the whole thing
	 */
	log.Printf("Exiting\n")
	os.Exit(0)
}

func abortNotify() {
	log.Printf("My job should be aborted...")
}

var Mode string

func main() {
	var config_path string

	log.Printf("Starting worker daemon\n")

	flag.StringVar(&config_path, "conf", "/etc/ansaas/worker-conf.yaml", "path to a config file")
	flag.StringVar(&Mode, "mode", "", "mode of operation")
	flag.Parse()

	switch Mode {
	case "docker":
		abortHandler = stopSelf
	default:
		abortHandler = abortNotify
	}

	err := ReadConfig(config_path, &conf)
	if err != nil {
		log.Printf("Bad config: %s\n", err.Error())
		return
	}

	err = dbConnect()
	if err != nil {
		log.Printf("Cannot connect to DB: %s\n", err.Error())
		return
	}

	mqStart()
}
