package main

import (
	"log"
	"time"
	"common"
	"gopkg.in/mgo.v2"
	"api"
)

var dbname = api.DBStateDB
var session *mgo.Session

func dbConnect() error {
	var err error

	dbc := xh.ParseXCreds(conf.DB)
	dbc.Resolve()
	if Mode != "devel" {
		dbc.UPEnv()
	}
	if dbc.Domn != "" {
		dbname = dbc.Domn
	}

	log.Printf("Connect to db %s@%s\n", dbc.User, dbc.Addr())
	info := mgo.DialInfo{
		Addrs:		[]string{dbc.Addr()},
		Database:	dbname,
		Timeout:	60 * time.Second,
		Username:	dbc.User,
		Password:	dbc.Pass,
	}

	session, err = mgo.DialWithInfo(&info);
	if err != nil {
		log.Printf("dbConnect: Can't dial to %s with db %s (%s)",
				conf.DB, dbname, err.Error())
		return err
	}

	session.SetMode(mgo.Monotonic, true)

	return nil

}

