package main

import (
	"context"
	"net/http"
	"xrest"
	"api"
	"log"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

func handleClusters(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	var params api.ClusterAdd
	return xrest.HandleMany(ctx, w, r, Clusters{}, &params)
}

func handleCluster(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	return xrest.HandleOne(ctx, w, r, Clusters{}, nil)
}

func handleClusterNodes(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	var nd api.NodeAdd
	return xrest.HandleMany(ctx, w, r, Nodes{cl.(*ClusterDesc)}, &nd)
}

func handleClusterNode(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	return xrest.HandleOne(ctx, w, r, Nodes{cl.(*ClusterDesc)}, nil)
}

func handleClusterSSHKey(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	return xrest.HandleProp(ctx, w, r, Clusters{}, &ClKeyProp{}, nil)
}

func handleClusterSSHKeyGen(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	x, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	cl := x.(*ClusterDesc)
	return cl.Keygen(ctx)
}

func handleDeploys(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	var da api.ClusterDeploy
	return xrest.HandleMany(ctx, w, r, Deploys{cl.(*ClusterDesc)}, &da)
}

func handleDeploy(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	return xrest.HandleOne(ctx, w, r, Deploys{cl.(*ClusterDesc)}, nil)
}

func handleDeployLogs(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	return xrest.HandleProp(ctx, w, r, Deploys{cl.(*ClusterDesc)}, DepLogsProp{}, nil)
}

var wsupgrader = websocket.Upgrader{}

func handleDeployLogsPull(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	log.Printf("Request for logs pull: %s\n", r.URL.Path)

	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	did := mux.Vars(r)["did"]
	les := make(chan *api.DepLogEntry)
	stop := make(chan struct{})
	defer close(stop)

	err := receiveLogs(did, les, stop)
	if err != nil {
		return DErrM(xrest.GenErr, "Cannot pull logs")
	}

	d, cerr := Deploys{cl.(*ClusterDesc)}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	dep := d.(*DeployDesc)
	c, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		return DErrM(xrest.GenErr, "Cannot make websocket")
	}

	cls := make(chan struct{})
	go func() {
		/* XXX SetCloseHandler() doesn't work */
		for {
			_, _, err := c.ReadMessage()
			if err == nil {
				continue
			}
			log.Printf("Client closes connection\n")
			close(cls)
			break
		}
	}()

	defer c.Close()

	/*
	 * If the sattus is not started we'll see nothing in mq and stuck.
	 * if not, the worker hasn't pushed the "fin" message before we
	 * subscribed to it.
	 */
	if dep.Status == api.DeployStarted {
		for {
			select {
			case  msg := <-les:
				if msg.Type == api.LogFinType {
					goto out
				}

				data, _ := json.Marshal(msg)
				c.WriteMessage(websocket.BinaryMessage, data)
			case <-cls:
				goto out
			}
		}
	}

out:
	c.WriteMessage(websocket.CloseMessage,
			websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	return nil
}

func handleArtifacts(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	return xrest.HandleProp(ctx, w, r, Deploys{cl.(*ClusterDesc)}, DepArtifactsProp{}, nil)
}

func handleArtifact(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	dep, cerr := Deploys{cl.(*ClusterDesc)}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	data, cerr := dep.(*DeployDesc).Artifact(ctx, mux.Vars(r)["aname"])
	if cerr != nil {
		return cerr
	}

	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write(data)

	return nil
}
