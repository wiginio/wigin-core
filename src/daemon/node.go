package main

import (
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"context"
	"strconv"
	"net/url"
	"xrest"
	"api"
)

type Nodes struct { cl *ClusterDesc }

type NodeDesc struct {
	Addr		string		`bson:"addr"`
	Port		uint		`bson:"port,omitempty"`
}

func (nd *NodeDesc)IP() string {
	r := nd.Addr
	if nd.Port != 0 {
		r += ":" + strconv.Itoa(int(nd.Port))
	}
	return r
}

func (nd *NodeDesc)info(ctx context.Context, _ *ClusterDesc, name string, details bool) (interface{}, *xrest.ReqErr) {
	ni := &api.NodeInfo {
		Name:	name,
		Addr:	nd.Addr,
		Port:	nd.Port,
	}

	return ni, nil
}

type Node struct {
	name	string
	d	*NodeDesc
	cl	*ClusterDesc
}

func (nd *Node)Add(ctx context.Context, p interface{}) *xrest.ReqErr {
	err := dbUpdatePart(ctx, nd.cl, bson.M{ "nodes." + nd.name: nd.d })
	if err != nil {
		return DErrD(err)
	}

	nd.cl.Nodes[nd.name] = nd.d
	return nil
}

func (nd *Node)Upd(ctx context.Context, upd interface{}) *xrest.ReqErr {
	return DErrM(xrest.GenErr, "Not updatable")
}

func (nd *Node)Del(ctx context.Context) *xrest.ReqErr {
	err := dbUpdatePartUn(ctx, nd.cl, bson.M{ "nodes." + nd.name: "" })
	if err != nil {
		return DErrD(err)
	}

	delete(nd.cl.Nodes, nd.name)
	return nil
}

func (nd *Node)Info(ctx context.Context, q url.Values, details bool) (interface{}, *xrest.ReqErr) {
	return nd.d.info(ctx, nd.cl, nd.name, details)
}

func (ns Nodes)Create(ctx context.Context, p interface{}) (xrest.Obj, *xrest.ReqErr) {
	params := p.(*api.NodeAdd)
	return &Node {
		name:	params.Name,
		cl:	ns.cl,
		d:	&NodeDesc {
			Addr:	params.Addr,
			Port:	params.Port,
		},
	}, nil
}

func (ns Nodes)Get(ctx context.Context, r *http.Request) (xrest.Obj, *xrest.ReqErr) {
	name := mux.Vars(r)["nname"]
	nd, ok := ns.cl.Nodes[name]
	if !ok {
		return nil, DErrM(xrest.BadRequest, "No such node")
	}

	return &Node{ name: name, cl: ns.cl, d: nd }, nil
}

func (ns Nodes)Iterate(ctx context.Context, q url.Values, cb func(context.Context, xrest.Obj) *xrest.ReqErr) *xrest.ReqErr {
	for nn, nd := range ns.cl.Nodes {
		cerr := cb(ctx, &Node{ name: nn, cl: ns.cl, d: nd })
		if cerr != nil {
			return cerr
		}
	}

	return nil
}
