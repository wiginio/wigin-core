package api

const (
	DbError		uint = 4	// Error requesting database (except NotFound)
	Duplicate	uint = 5	// ID duplication
	NotFound	uint = 6	// No resource found
	NotAvailable	uint = 7	// Operation not possible on this object
)

const (
	DeployStarted	string = "started"
	DeploySucceeded	string = "succeeded"
	DeployFailed	string = "failed"
	DeployAborted	string = "aborted"
)

type UserLogin struct {
	Name	string			`json:"username"`
	Pass	string			`json:"password"`
}

type ClusterAdd struct {
	Name	string			`json:"name"`
}

type ClusterInfo struct {
	Id	string			`json:"id"`
	Name	string			`json:"name"`
	LD	*ClusterDeployInfo	`json:"last_deploy,omitempty"`
	Nodes	[]*NodeInfo		`json:"nodes,omitempty"`
}

type NodeAdd struct {
	Name	string			`json:"name"`
	Addr	string			`json:"addr"`
	Port	uint			`json:"port,omitempty"`
}

type NodeInfo struct {
	Name	string			`json:"name"`
	Addr	string			`json:"addr"`
	Port	uint			`json:"port,omitempty"`
}

type ClusterKeyInfo struct {
	Key	string			`json:"key,omitempty"`
}

type ClusterDeploy struct {
	Playbook	string		`json:"playbook"`
	Hosts		string		`json:"hosts_ini_base64"`
}

type ClusterDeployInfo struct {
	Id		string		`json:"id"`
	Playbook	string		`json:"playbook"`
	Started		string		`json:"started"`
	Status		string		`json:"status"`
	Finished	string		`json:"finished,omitempty"`
}

type LogRecord struct {
	Ts	string	`json:"ts"`
	Type	string	`json:"type"`
	Text	string	`json:"text"`
}

