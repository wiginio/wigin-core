BINS := daemon worker
IMG_PREFIX ?= ansaas/

all: $(BINS)

define gen-gobuild
$(1): ansaas-$(1)

$(1)-img: $(1)
	cp ansaas-$(1) docker/$(1)/
	cd docker/$(1)/ && docker build --network=host  -t $(IMG_PREFIX)$(1) .

ansaas-$(1): .FORCE
	go build -o ansaas-$(1) $(1)
endef

.PHONY: .FORCE

$(foreach b,$(BINS),$(eval $(call gen-gobuild,$b)))
